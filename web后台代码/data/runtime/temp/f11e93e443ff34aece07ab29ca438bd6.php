<?php /*a:1:{s:71:"/www/wwwroot/dati.sdwanyue.com/public/themes/default/wap/home/news.html";i:1646978699;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	<title>基层动态</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="referrer" content="origin">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta content="telephone=no" name="format-detection" />
	<!-- Set render engine for 360 browser -->
	<meta name="renderer" content="webkit">

	<!-- No Baidu Siteapp-->
	<meta http-equiv="Cache-Control" content="no-siteapp"/>
	<link href="/static/appapi/css/page.css?t=1577502879" rel="stylesheet">
	<script type="text/javascript" src="/static/wxshare/share/js/jquery-1.10.1.min.js"></script>
	<script type="text/javascript" src="/static/js/jquery.js"></script>
	<script type="text/javascript" src="/static/js/layer/layer.js"></script>
	<style>
		video{
			width: 100%;
		}
		body{
			overflow-x: hidden;
			word-break: break-all;
		}
	</style>
</head>

<body class="body-white">
<h1 class="title"><?php echo $info['title']; ?></h1>
<h6 class="time" style="font-size:14px">
	<span style="font-weight: 400;"><?php echo $info['author']; ?>&nbsp;&nbsp;&nbsp;<?php echo date('Y-m-d H:i',$info['addtime']); ?></span>
	<span style="float: right;color: #959595;font-weight: 400;">阅读次数：<?php echo $info['view']; ?></span></h6>
<div class="container tc-main">
	<div class="page_content">
		<?php echo $info['content']; ?>
	</div>
	<input type="hidden" id="nid" name="nid" value="<?php echo $info['id']; ?>">
</div>
<!-- /container -->
<div class="ping" id="ping">
	<div class="ping-tit">
		<span class="ping-tit-sapn span-zan choose">赞(<icon class="num"><?php echo $countzan; ?></icon>)</span>
	</div>
	<div class="ping-main">
		<?php if(is_array($pinginfo) || $pinginfo instanceof \think\Collection || $pinginfo instanceof \think\Paginator): if( count($pinginfo)==0 ) : echo "" ;else: foreach($pinginfo as $key=>$vo): ?>

			<div class="ping-con">

				<div  class="ping-con-avatar"><img src="<?php echo $vo['userinfo']['avatar']; ?>" /></div>
				<div class="ping-con-main">
					<p class="ping-con-name"><?php echo $vo['userinfo']['user_nickname']; ?> | <?php echo $vo['userinfo']['branch']; ?></p>
					<p class="ping-con-time"><?php echo $vo['addtime']; ?></p>
					<p class="ping-con-content"><?php echo $vo['content']; ?></p>
				</div>
			</div>

		<?php endforeach; endif; else: echo "" ;endif; ?>

	</div>
</div>


<div class="bottom-tc"></div>

<div class="js-bottom">
	<?php if($small_switch == 0): ?>
		<span style="width: 74%; display: inline-block;"></span>
	<?php endif; ?>
	<span class="span-icon btn-zan">
			<?php if($zinfo['zan'] == 1): ?>
			<img src="/static/appapi/images/zan_c.png" />
			<?php else: ?>
			<img src="/static/appapi/images/zan.png" />
			<?php endif; ?>

		</span>
	<?php if($small_switch == 0): ?>
			<span class="span-icon btn-collect">
				<?php if($zinfo['collect'] == 1): ?>
				<img src="/static/appapi/images/star_c.png" />
				<?php else: ?>
				<img src="/static/appapi/images/star.png" />
				<?php endif; ?>
			</span>
	<?php endif; ?>

</div>
<?php if($small_switch == 0): ?>
	<div class="input-main">
		<div class="input-center">
			<textarea class="msg" placeholder="说点什么吧..."></textarea>
			<button class="cancel">取消</button>
			<button class="postmsg">发表</button>
		</div>
		<div class="input-other"></div>
	</div>
<?php endif; ?>


<script src="/static/appapi/js/jweixin-1.3.2.js"></script>
<script>
	var id = <?php echo $info['id']; ?>;
	var uid = <?php echo $uid; ?>;
	var type = <?php echo $type; ?>;
	var token = '<?php echo $token; ?>';
	$('.span-c').click(function(){
		$('.input-main').fadeIn();
		$(".input-center").animate({top:'0px'});

	});
	$('.input-other').click(function(){
		$('.input-main').fadeOut();
		$(".input-center").animate({top:'-210px'})

	});
	$('.cancel').click(function(){
		$('.input-main').fadeOut();
		$(".input-center").animate({top:'-210px'})

	});
	$('.postmsg').click(function(){
		var content = $('.msg').val();
		if(content ===''){
			layer.msg('内容不能为空');
		}
		var url = "/wap/home/msgpost";
		$.ajax({
			type: 'POST',
			url: url,
			data:{'id':$("#nid").val(),'uid':uid,'token':token,'content':content,'type':type},
			dataType:'json',
			success: function(data){
				if(data==700){
					wx.miniProgram.redirectTo({
						url:'/pages/login/index'
					})
					return ;
				}
				if(data.code==0){
					layer.msg(data.msg);
				}
				if(data.code!=0){
					layer.msg(data.msg);
					setTimeout(function(){ location.reload(); }, 1500);
				}
			},
			error: function(data){
				layer.msg('网络错误，请稍后重试');
			}
		});


	});
	$('.btn-msg').click(function(){
		location.href = "#ping";
	});
	$('.btn-zan').click(function(){
		var url = "/wap/home/zan";
		$.ajax({
			type: 'POST',
			url: url,
			data:{'id':id,'uid':uid,'token':token,'type':type},
			dataType:'json',
			success: function(data){
				if(data==700){
					wx.miniProgram.redirectTo({
						url:'/pages/login/index'
					})
					return ;
				}
				if(data.code==0){
					layer.msg(data.msg);
				}
				if(data.code!=0){
					layer.msg(data.msg);
					setTimeout(function(){ location.reload(); }, 1500);
				}
			},
			error: function(data){
				layer.msg('网络错误，请稍后重试');
			}
		});
	});
	$('.btn-collect').click(function(){
		var url = "/wap/home/collect";
		$.ajax({
			type: 'POST',
			url: url,
			data:{'id':id,'uid':uid,'token':token,'type':type},
			dataType:'json',
			success: function(data){

				if(data==700){
					wx.miniProgram.redirectTo({
						url:'/pages/login/index'
					})
					return ;
				}

				if(data.code==0){
					layer.msg(data.msg);
				}
				if(data.code!=0){
					layer.msg(data.msg);
					setTimeout(function(){ location.reload(); }, 1500);
				}
			},
			error: function(data){
				layer.msg('网络错误，请稍后重试');
			}
		});
	});


	$('.span-ping11').click(function(){
		var _this = this;
		var url = "/wap/home/countping";
		$.ajax({
			type: 'POST',
			url: url,
			data:{'id':id,'type':type},
			dataType:'json',
			success: function(data){
				if(data.code==0){
					layer.msg(data.msg);
				}else{
					$('.ping-tit-sapn').removeClass('choose');
					$(_this).addClass('choose');
					var html = '';
					var info = data.msg;
					for(i=0;i<info.length;i++){
						html +='<div class="ping-con"> \
										<div  class="ping-con-avatar"><img src="'+info[i].userinfo.avatar+'" /></div> \
										<div class="ping-con-main"> \
											<p class="ping-con-name">'+info[i].userinfo.user_nickname+' | '+info[i].userinfo.branch+'</p> \
											<p class="ping-con-time">'+info[i].addtime+'</p> \
											<p class="ping-con-content">'+info[i].content+'</p> \
										</div> \
									</div>';
					}
					$('.ping-main').html(html);
				}
			},
			error: function(data){
				layer.msg('网络错误，请稍后重试');
			}
		});
	});
	$('.span-zan11').click(function(){
		var _this = this;
		var url = "/wap/home/countzan";
		$.ajax({
			type: 'POST',
			url: url,
			data:{'id':id,'type':type},
			dataType:'json',
			success: function(data){
				if(data.code==0){
					layer.msg(data.msg);
				}else{
					$('.ping-tit-sapn').removeClass('choose');
					$(_this).addClass('choose');
					var html = '';
					var info = data.msg;
					console.log(info);
					for(i=0;i<info.length;i++){
						html +='<div class="ping-con"> \
										<div  class="ping-con-avatar"><img src="'+info[i].userinfo.avatar+'" /></div> \
										<div class="ping-con-main"> \
											<p class="ping-con-name">'+info[i].userinfo.user_nickname+' | '+info[i].userinfo.branch+'</p> \
											<p class="ping-con-time">'+info[i].addtime+'</p> \
										</div> \
									</div>';
					}
					$('.ping-main').html(html);

				}
			},
			error: function(data){
				layer.msg('网络错误，请稍后重试');
			}
		});
	});


</script>
























</body>
</html>